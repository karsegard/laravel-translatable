<?php


if (!function_exists('lang_url')) {
    function lang_url($lang)
    {
        $query = request()->query();
        $query['lang'] = $lang;
        return request()->fullUrlWithQuery($query);
    }
}
