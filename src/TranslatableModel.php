<?php

namespace KDA\Laravel\Translatable;

use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\Translatable\Facades\Translatable;
use Str;
use Kirschbaum\PowerJoins\PowerJoins;

abstract class TranslatableModel extends Model
{
    use PowerJoins; 
    protected ?array $translatable_locales = null;
    protected ?string $translation_locale = null;
    protected ?string $fallback_locale = null;
    protected array $localizable = [];
    protected bool $eagerLoadTranslations = true;
    protected bool $hideLocaleSpecificAttributes = true;
    protected bool $appendLocalizedAttributes = true;
    protected string $translatableRelationship;
    protected array $queuedTranslations = [];

    protected bool $translation_fallen_back = false;


    protected static function boot()
    {
        parent::boot();
        static::created(function ($model) {
            $model->createQueuedTranslations();
        });

        static::updated(function ($model) {
            $model->createQueuedTranslations();
            $model->translations->each(fn ($tr) => $tr->save());
        });
    }
    public function getTranslatableLocales():array
    {
        return $this->translatable_locales ?? Translatable::getTranslatableLocales();
    }
    public function getTranslatableLocalesCount():int
    {
        return count($this->getTranslatableLocales());
    }

    public function getTranslatableAttributes()
    {
        return $this->localizable;
    }

    public function createQueuedTranslations()
    {
        foreach ($this->queuedTranslations as $locale => $attributes) {
            $translationModel = $this->getTranslationModel();
            $translation = new $translationModel($attributes);
            $translation->locale = $locale;
            $relationship = $this->getTranslatableRelationship();
            $translation->$relationship()->associate($this);
            $translation->save();
        }
    }

    public function getTranslatableRelationship()
    {
        return $this->translatableRelationship;
    }

    public function getTranslationModel(): string
    {
        $modelName = class_basename(get_class($this));
        return "App\\Models\\{$modelName}Translation";
    }

    public function getLocale()
    {
        return $this->translation_locale ?? app()->getLocale();
    }

    public function setFallbackLocale(string | null $locale): static{
        $this->fallback_locale = $locale;
        return $this;
    }
    public function getFallbackLocale (): ?string
    {
        return $this->fallback_locale;
    }

    public function setLocale($locale): static
    {
        $this->unsetRelation('translation');
        $this->translation_locale = $locale;
        return $this;
    }

    public function __construct($attributes = [])
    {
        if ($this->eagerLoadTranslations) {
            $this->with[] = 'translations';
        }
        if ($this->hideTranslations) {
            $this->hidden[] = 'translations';
        }
        // We dynamically append localizable attributes to array output
        if ($this->appendLocalizedAttributes) {
            foreach ($this->localizable as $localizableAttribute) {
                $this->appends[] = $localizableAttribute;
            }
        }
        parent::__construct($attributes);
    }

    public function hasTranslationFallback():bool{
        return $this->translation_fallen_back;
    }

    public function getAttribute($attribute)
    {
        if (in_array($attribute, $this->localizable)) {
            if(isset($this->queuedTranslations[$this->getLocale()][$attribute])){
                return $this->queuedTranslations[$this->getLocale()][$attribute];
            }
            $translated_attribute =  $this->translations
                ->where('locale', $this->getLocale());
            
            if($translated_attribute->count()==0 && ($fallback = $this->getFallbackLocale()) && !blank($fallback)){
                $this->translation_fallen_back =  true;
                $translated_attribute =  $this->translations
                ->where('locale',$fallback );
                //fallback ? 
            }

            return $translated_attribute ->first()?->{$attribute} ?? null;
        }
        return parent::getAttribute($attribute);
    }

    public function setAttribute($attribute, $value)
    {
        //   foreach ($this->localizable as $localizableAttribute) {
        if (in_array($attribute, $this->localizable)) {
            $translation =  $this->translations
                ->where('locale', $this->getLocale())
                ->first();

            if (!$this->exists || !$translation) {
                $this->updated_at = now();
                $this->queuedTranslations[$this->getLocale()][$attribute] = $value;
                return;
            }
            $this->updated_at = now(); // making this one dirty
            $translation->{$attribute} = $value;
            return;
        }
        // }
        return parent::setAttribute($attribute, $value);
    }

    public function __call($method, $arguments)
    {
        // We handle the accessor calls for all our localizable attributes
        // e.g. getNameAttribute()
        foreach ($this->localizable as $localizableAttribute) {
            if ($method === 'get' . Str::studly($localizableAttribute) . 'Attribute') {
                return $this->{$localizableAttribute};
            }
        }
        return parent::__call($method, $arguments);
    }

    public function translations()
    {
        return $this->hasMany($this->getTranslationModel());
    }

    /*public function translation(){
        return $this->hasOne($this->getTranslationModel())->where('locale',$this->getLocale())->limit(1);
    }*/
    public function translation(){
        return $this->hasOne($this->getTranslationModel())->ofMany(['id'=>'max'],function($q){
            return $q->where('locale',$this->getLocale());
        });
    }
  /*  public function getTranslationAttribute(){
        return $this->translations->where('locale',$this->getLocale())->first();
    }
*/
    public function scopePartialyTranslated($q){
        return $q->whereHas('translations', operator: '<', count: $this->getTranslatableLocalesCount());
       // return $q->withCount('translations')->having('translations_count','<',$this->getTranslatableLocalesCount());
    }
    public function scopeFullyTranslated($q){
        return $q->whereHas('translations', operator: '>=', count: $this->getTranslatableLocalesCount());
       // return $q->withCount('translations')->having('translations_count','>=',$this->getTranslatableLocalesCount());
    }
    public function getIsFullyTranslatedAttribute(){
        return $this->translations->count() >= $this->getTranslatableLocalesCount();
    }
    public function getIsPartialyTranslatedAttribute(){
        return $this->translations->count() < $this->getTranslatableLocalesCount();
    }

    public function scopeForLocale($q,$locale){
        return $q->whereHas('translations',function($q)use($locale){
            return $this->where('locale','fr');
        });
    }

    public function scopeOrderedByTranslation($q,$field,$direction="asc"){
   //     dump( $q->joinRelationship('translation')->orderByPowerJoins('translation.'.$field,$direction)->toSql());
        return $q->joinRelationship('translation',fn($join)=>$join->where('locale',$this->getLocale()))->orderByPowerJoins('translation.'.$field,$direction);
    }
}
