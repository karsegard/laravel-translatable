<?php
namespace KDA\Laravel\Translatable;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasHelper;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Translatable\Facades\Translatable as Facade;
use KDA\Laravel\Translatable\Translatable as Library;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasHelper;
    protected $packageName ='laravel-translatable';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
