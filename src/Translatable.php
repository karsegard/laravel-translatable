<?php
namespace KDA\Laravel\Translatable;

//use Illuminate\Support\Facades\Blade;
class Translatable 
{
    
    protected array $translatableLocales = ['fr','en'];

    public function translatableLocales(array $locales):static
    {
        $this->translatableLocales = $locales;
        return $this;
    }

    public function getTranslatableLocales(){
        return $this->translatableLocales;
    }

}